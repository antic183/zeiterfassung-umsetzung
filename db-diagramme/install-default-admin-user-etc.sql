use zeiterfassung;

# INITIALISIERE DIE DEFAULT DATEN FüR DIE APP:

# *** ADMIN USER ***
# username (email)= admin
# password = 1234

-- -----------------------------------------------------
-- INSERT DEFAULT Account types
-- -----------------------------------------------------
insert into leistungsart(name) values("Normalarbeitszeit"), ("Krankheit"), ("Ferien"), ("Nachtarbeit");

-- -----------------------------------------------------
-- INSERT DEFAULT GROUPS admin AND user
-- -----------------------------------------------------
insert into Gruppe (name) values("admin"), ("user");
-- select * from Gruppe;

-- -----------------------------------------------------
-- INSERT DEFAULT RIGHTS ADMIN AND USER ON TABLE RECHT
-- -----------------------------------------------------
INSERT INTO recht (Recht, Gruppe_id) values (1, 1), (0, 2);
-- select * from recht;

-- -----------------------------------------------------
-- INSERT DEFAULT TEAM
-- -----------------------------------------------------
INSERT INTO team(name) value("default");
-- select * from team;

-- -----------------------------------------------------
-- INSERT DEFAULT ORT
-- -----------------------------------------------------
INSERT INTO ort(plz, ort) values("5000", "Aarau");
-- select * from ort;

-- -----------------------------------------------------
-- INSERT DEFAULT Rolle
-- -----------------------------------------------------
INSERT INTO rolle(name) values("default Rolle"),("Mitarbeiter"),("Software Architekt"), ("Projektleiter");
-- select * from rolle;

-- -----------------------------------------------------
-- INSERT DEFAULT ADMIN User. For first App checkLogin
-- -----------------------------------------------------
INSERT INTO person (Personalnummer, Name, Vorname, Email, Password, Strasse, Hausnummer, Gruppe_id, Ort_id, team_id) 
values("a-144.547-4-G", "App", "Admin", "admin", SHA2(1234, 512), "admin-street", "6a", 1, 1, 1);

-- -----------------------------------------------------
-- INSERT DEFAULT Rolle_Person
-- -----------------------------------------------------
INSERT INTO rolle_person(Default_Rolle, Rolle_id, Person_id) values(1, 1, 1);
-- select * from rolle_person;