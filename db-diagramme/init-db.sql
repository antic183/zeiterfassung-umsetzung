drop database if exists zeiterfassung;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema zeiterfassung
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zeiterfassung` DEFAULT CHARACTER SET utf8 ;
USE `zeiterfassung` ;

-- -----------------------------------------------------
-- Table `zeiterfassung`.`Team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Team` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Beschreibung` VARCHAR(250) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Ort`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Ort` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `PLZ` VARCHAR(45) NOT NULL,
  `Ort` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Gruppe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Gruppe` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Personalnummer` VARCHAR(15) NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Vorname` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(128) NOT NULL,
  `Strasse` VARCHAR(45) NULL,
  `Hausnummer` VARCHAR(45) NULL,
  `Team_id` INT NOT NULL,
  `Ort_id` INT NOT NULL,
  `Gruppe_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Gruppe_id`, `Team_id`, `Ort_id`),
  INDEX `fk_Person_Team` (`Team_id` ASC),
  INDEX `fk_Person_Ort` (`Ort_id` ASC),
  INDEX `fk_Person_Gruppe` (`Gruppe_id` ASC),
  UNIQUE INDEX `Personalnummer_UNIQUE` (`Personalnummer` ASC),
  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC),
  CONSTRAINT `fk_Person_Team`
    FOREIGN KEY (`Team_id`)
    REFERENCES `zeiterfassung`.`Team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Person_Ort`
    FOREIGN KEY (`Ort_id`)
    REFERENCES `zeiterfassung`.`Ort` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Person_Gruppe`
    FOREIGN KEY (`Gruppe_id`)
    REFERENCES `zeiterfassung`.`Gruppe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Zeitkonto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Zeitkonto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Stundenkontingent` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Projekt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Projekt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Projektnummer` VARCHAR(15) NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Beschreibung` VARCHAR(250) NOT NULL,
  `Parent_id` INT NULL DEFAULT NULL,
  `Zeitkonto_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Zeitkonto_id`),
  UNIQUE INDEX `Projektnummer_UNIQUE` (`Projektnummer` ASC),
  INDEX `fk_Projekt_Parent` (`Parent_id` ASC),
  INDEX `fk_Projekt_Zeitkonto` (`Zeitkonto_id` ASC),
  CONSTRAINT `fk_Projekt_Parent`
    FOREIGN KEY (`Parent_id`)
    REFERENCES `zeiterfassung`.`Projekt` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Projekt_Zeitkonto`
    FOREIGN KEY (`Zeitkonto_id`)
    REFERENCES `zeiterfassung`.`Zeitkonto` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Rolle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Rolle` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Rolle_Person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Rolle_Person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Default_Rolle` TINYINT(1) NOT NULL DEFAULT 0,
  `Rolle_id` INT NOT NULL,
  `Person_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Rolle_id`, `Person_id`),
  INDEX `fk_Rolle_Person_Rolle` (`Rolle_id` ASC),
  INDEX `fk_Rolle_Person_Person` (`Person_id` ASC),
  UNIQUE INDEX `unique_Rolle_Person` (`Person_id` ASC, `Rolle_id` ASC),
  CONSTRAINT `fk_Rolle_Mitarbeiter`
    FOREIGN KEY (`Rolle_id`)
    REFERENCES `zeiterfassung`.`Rolle` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Rolle_Mitarbeiter_Person`
    FOREIGN KEY (`Person_id`)
    REFERENCES `zeiterfassung`.`Person` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Rolle_Person_Projekt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Rolle_Person_Projekt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Rolle_Person_id` INT NOT NULL,
  `Projekt_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Rolle_Person_id`, `Projekt_id`),
  INDEX `fk_P_Projekt_id` (`Projekt_id` ASC),
  INDEX `fk_RP_Rolle_Person_id` (`Rolle_Person_id` ASC),
  UNIQUE INDEX `unique_Rolle_Person_Projekt` (`Rolle_Person_id` ASC, `Projekt_id` ASC),
  CONSTRAINT `fk_P_Projekt_id`
    FOREIGN KEY (`Projekt_id`)
    REFERENCES `zeiterfassung`.`Projekt` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_RP_Rolle_Person_id`
    FOREIGN KEY (`Rolle_Person_id`)
    REFERENCES `zeiterfassung`.`Rolle_Person` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Leistungsart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Leistungsart` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Zeitbuchung`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Zeitbuchung` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Datum` DATE NOT NULL,
  `Beschreibung` VARCHAR(250) NOT NULL,
  `Anzahl_Stunden` DECIMAL(4,2) NOT NULL,
  `Rolle_Person_Projekt_id` INT NOT NULL,
  `Leistungsart_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Rolle_Person_Projekt_id`, `Leistungsart_id`),
  INDEX `fk_Rolle_Person_Projekt_id` (`Rolle_Person_Projekt_id` ASC),
  INDEX `fk_Leistungsart_id` (`Leistungsart_id` ASC),
  CONSTRAINT `fk_Rolle_Person_Projekt_id`
    FOREIGN KEY (`Rolle_Person_Projekt_id`)
    REFERENCES `zeiterfassung`.`Rolle_Person_Projekt` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Leistungsart_id`
    FOREIGN KEY (`Leistungsart_id`)
    REFERENCES `zeiterfassung`.`Leistungsart` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zeiterfassung`.`Recht`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zeiterfassung`.`Recht` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Recht` TINYINT(1) NOT NULL,
  `Gruppe_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Gruppe_id`),
  INDEX `fk_Recht_Gruppe` (`Gruppe_id` ASC),
  CONSTRAINT `fk_Recht_Gruppe`
    FOREIGN KEY (`Gruppe_id`)
    REFERENCES `zeiterfassung`.`Gruppe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
