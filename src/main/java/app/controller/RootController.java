package app.controller;

import app.bll.AuthentificationBll;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class RootController {

    @FXML
    private TextField txtFieldUserName;
    @FXML
    private PasswordField pwdFieldPassword;

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
    }


    @FXML
    private void btnLoginClicked() throws InterruptedException {
        try {
            String username = txtFieldUserName.getText();
            String password = pwdFieldPassword.getText();
            if (AuthentificationBll.checkLogin(username, password)) {
                System.out.println("Passwort OK! AppView wird geladen!");

                MenuContentLoader contentViewLoader = new MenuContentLoader();
                contentViewLoader.initNavigationLayout();
                contentViewLoader.showAppView();
                contentViewLoader.setAppContent("Report.fxml");
            } else {
                System.out.println("falsches Passwort!");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
