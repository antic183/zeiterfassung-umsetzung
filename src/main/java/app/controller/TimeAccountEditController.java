package app.controller;

import app.bll.TimeAccountBll;
import app.model.TimeAccount;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class TimeAccountEditController {

    @FXML
    private TextField txtName;
    @FXML
    private TextField txtMaxNumberOfHours;

    private TimeAccountBll timeAccountBll = new TimeAccountBll();

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
    }

    @FXML
    private void btnSaveClicked() {
        String name = txtName.getText();
        String maxNumberOfHours = txtMaxNumberOfHours.getText();

        if (timeAccountBll.timeAccountIsValid(name, maxNumberOfHours)) {
            TimeAccount timeAccount = new TimeAccount(name, Float.parseFloat(maxNumberOfHours));
            try {
                timeAccountBll.saveTimeAccount(timeAccount);
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
            new ModalLoader().closeDialog();
        } else {
            AlertHelper.showError("Error", "Fehlerhafte Eingabe");
        }
    }
}
