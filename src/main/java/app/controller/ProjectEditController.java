package app.controller;

import app.bll.ProjectBll;
import app.bll.TimeAccountBll;
import app.controller.cell_renderer.ProjectListCellRenderer;
import app.controller.cell_renderer.TimeAccountListCellRenderer;
import app.model.Project;
import app.model.TimeAccount;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;


/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class ProjectEditController {

    @FXML
    private TextField txtProjectNumber;
    @FXML
    private TextField txtName;
    @FXML
    private TextArea txtAreaDescription;
    @FXML
    private ComboBox<Project> comboParentProject;
    @FXML
    private ComboBox<TimeAccount> comboTimeAccount;

    private TimeAccountBll timeAccountBll = new TimeAccountBll();
    private ProjectBll projectBll = new ProjectBll();

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
        initComboCellRenderers();
        initComboTimeAccount();
        initComboParentProject();
    }

    private void initComboCellRenderers() {
        comboParentProject.setButtonCell(new ProjectListCellRenderer());
        comboParentProject.setCellFactory(new Callback<ListView<Project>, ListCell<Project>>() {
            @Override
            public ListCell<Project> call(ListView<Project> param) {
                return new ProjectListCellRenderer();
            }
        });

        comboTimeAccount.setButtonCell(new TimeAccountListCellRenderer());
        comboTimeAccount.setCellFactory(new Callback<ListView<TimeAccount>, ListCell<TimeAccount>>() {
            @Override
            public ListCell<TimeAccount> call(ListView<TimeAccount> param) {
                return new TimeAccountListCellRenderer();
            }
        });
    }

    private void initComboTimeAccount() {
        comboTimeAccount.setItems(timeAccountBll.getAllTimeAccounts());
    }

    private void initComboParentProject() {
        if (projectBll.hasProjects()) {
            comboParentProject.setItems(projectBll.getAllProjects());
        } else {
            comboParentProject.setDisable(true);
        }
    }

    @FXML
    private void btnSaveClicked() {
        String projectNumber = txtProjectNumber.getText();
        String name = txtName.getText();
        String description = txtAreaDescription.getText();
        Project parentProject = comboParentProject.getValue();
        TimeAccount timeAccount = comboTimeAccount.getValue();

        Project project = new Project(name, projectNumber, description);
        project.setParent(parentProject);
        project.setTimeAccount(timeAccount);

        if (projectBll.projectIsValid(project)) {
            try {
                projectBll.saveProject(project);
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
            new ModalLoader().closeDialog();
        } else {
            AlertHelper.showError("Error", "Fehlerhafte Eingabe");
        }
    }
}
