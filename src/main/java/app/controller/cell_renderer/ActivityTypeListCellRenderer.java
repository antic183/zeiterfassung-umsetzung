package app.controller.cell_renderer;

import app.model.ActivityType;
import javafx.scene.control.ListCell;

/**
 * Created by Antic Marjan on 11.03.2017.
 */
public class ActivityTypeListCellRenderer extends ListCell<ActivityType> {
    @Override
    protected void updateItem(ActivityType item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            setGraphic(null);
            setText(null);
        } else {
            setText(item.getName());
        }
    }
}
