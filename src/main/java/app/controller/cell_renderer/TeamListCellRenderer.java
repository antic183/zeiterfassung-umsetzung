package app.controller.cell_renderer;

import app.model.Team;
import javafx.scene.control.ListCell;

/**
 * Created by Antic Marjan on 11.03.2017.
 */
public class TeamListCellRenderer extends ListCell<Team> {
    @Override
    protected void updateItem(Team item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            setGraphic(null);
            setText(null);
        } else {
            setText(item.getName());
        }
    }
}
