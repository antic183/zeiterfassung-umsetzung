package app.controller;

import app.bll.TeamBll;
import app.model.Team;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Created by Antic Marjan on 13.03.2017.
 */
public class TeamEditController {
    @FXML
    private TextField txtName;
    @FXML
    private TextArea txtAreaDescription;

    private TeamBll teamBll = new TeamBll();

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
    }

    @FXML
    private void btnSaveClicked() {
        String name = txtName.getText();
        String description = txtAreaDescription.getText();
        Team team = new Team(name, description);

        if (teamBll.teamIsValid(team)) {
            try {
                teamBll.saveTeam(team);
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
            new ModalLoader().closeDialog();
        } else {
            AlertHelper.showError("Error", "Fehlerhafte Eingabe");
        }
    }
}
