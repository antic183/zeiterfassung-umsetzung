package app.controller;

import app.bll.PersonBll;
import app.bll.TeamBll;
import app.model.EAccountType;
import app.model.Person;
import app.model.Team;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Optional;

/**
 * Created by Antic Marjan on 09.03.2017.
 */
public class PersonController {

    @FXML
    private TableView<Person> personTable;
    @FXML
    private TableColumn<Person, String> personPersonnelNumberColumn;
    @FXML
    private TableColumn<Person, String> personFirstnameColumn;
    @FXML
    private TableColumn<Person, String> personLastnameColumn;
    @FXML
    private TableColumn<Person, String> personStreetColumn;
    @FXML
    private TableColumn<Person, String> personCityColumn;
    @FXML
    private TableColumn<Person, String> personDefaultRoleColumn;
    @FXML
    private TableColumn<Person, String> personEmailColumn;
    @FXML
    private TableColumn<Person, String> personPasswordColumn;
    @FXML
    private TableColumn<Person, EAccountType> personAccountTypeColumn;

    @FXML
    private TableView<Team> teamTable;
    @FXML
    private TableColumn<Team, String> teamNameColumn;
    @FXML
    private TableColumn<Team, String> teamDescriptionColumn;
    @FXML
    private TableColumn<Team, String> teamRelatedPersonColumn;

    private PersonBll personBll = new PersonBll();
    private TeamBll teamBll = new TeamBll();

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
        initTeamTableColumns();
        initTeamTableContextMenu();
        initTeams();
        initPersonTableColumns();
        initPersonTableContextMenu();
        initPersons();

    }

    private void initTeams() {
        teamTable.setItems(teamBll.getAllTeams());
    }

    private void initTeamTableContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Löschen");
        MenuItem editMenuItem = new MenuItem("Bearbeiten");
        menu.getItems().add(removeMenuItem);
        menu.getItems().add(editMenuItem);
        teamTable.setContextMenu(menu);

        removeMenuItem.setOnAction((event) -> {
            Team selectedTeam = teamTable.getSelectionModel().getSelectedItem();
            try {
                String msg = "Wirklich löschen?";
                Optional<ButtonType> result = AlertHelper.showConfirm("Bestätige", msg);
                if (result.get() == ButtonType.OK) {
                    try {
                        teamBll.deleteTeam(selectedTeam);
                    } catch (Exception e) {
                        AlertHelper.showError("Error", e.getMessage());
                    }
                    initTeams(); // Todo: unschön, ev. refactoring
                }
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
        });

        editMenuItem.setOnAction((event) -> {
            // Todo: Edit implementieren
            Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
            AlertHelper.showInfo("App-Info", "wurde nicht implementiert");
            // selectedProject muss an den Controller übermittelt werden
            // new ModalLoader().showModal("ProjectEditModal.fxml"); // bei edit saveProject in BLL aufrufen
        });
    }

    private void initTeamTableColumns() {
        teamNameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        teamDescriptionColumn.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        teamRelatedPersonColumn.setCellValueFactory(cellData -> cellData.getValue().personListProperty().asString());
    }

    private void initPersons() {
        personTable.setItems(personBll.getAllPersons());
    }

    private void initPersonTableContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Löschen");
        MenuItem editMenuItem = new MenuItem("Bearbeiten");
        menu.getItems().add(removeMenuItem);
        menu.getItems().add(editMenuItem);
        personTable.setContextMenu(menu);

        removeMenuItem.setOnAction((event) -> {
            Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
            try {
                String msg = "Wirklich löschen?";
                Optional<ButtonType> result = AlertHelper.showConfirm("Bestätige", msg);
                if (result.get() == ButtonType.OK) {
                    personBll.deleteProject(selectedPerson);
                    initPersons(); // Todo: unschön, ev. refactoring
                }
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
        });

        editMenuItem.setOnAction((event) -> {
            // Todo: Edit implementieren
            Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
            AlertHelper.showInfo("App-Info", "wurde nicht implementiert");
            // selectedProject muss an den Controller übermittelt werden
            // new ModalLoader().showModal("ProjectEditModal.fxml"); // bei edit saveProject in BLL aufrufen
        });
    }

    private void initPersonTableColumns() {
        personPersonnelNumberColumn.setCellValueFactory(cellData -> cellData.getValue().personnelNumberProperty());
        personFirstnameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        personLastnameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        personStreetColumn.setCellValueFactory(cellData -> cellData.getValue().fullStreetProperty());
        personCityColumn.setCellValueFactory(cellData -> cellData.getValue().fullCityProperty());
        personEmailColumn.setCellValueFactory(cellData -> cellData.getValue().emailProperty());
        personPasswordColumn.setCellValueFactory(cellData -> cellData.getValue().passwordProperty());

        personDefaultRoleColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().defaultRoleProperty().getValue() == null
                    ? null : cellData.getValue().defaultRoleProperty().getValue().nameProperty();
        });
        personAccountTypeColumn.setCellValueFactory(cellData -> cellData.getValue().accountTypeProperty());
    }

    @FXML
    private void btnAddNewPersonClicked() {
        if (teamBll.hasTeams()) {
            new ModalLoader().showModal("PersonEditModal.fxml");
            initTeams();
        } else {
            String msg = "Um ein Person anlegen zu können, muss ein Team existieren, dem die Person zugewiesen werden kann.";
            AlertHelper.showInfo("Information", msg);
        }
    }

    @FXML
    private void btnAddNewTeamClicked() {
        // Todo: ...
        new ModalLoader().showModal("TeamEditModal.fxml");
    }
}
