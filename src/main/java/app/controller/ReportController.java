package app.controller;

import app.bll.*;
import app.controller.cell_renderer.ActivityTypeListCellRenderer;
import app.controller.cell_renderer.ProjectListCellRenderer;
import app.controller.cell_renderer.RoleListCellRenderer;
import app.model.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class ReportController {

    @FXML
    private ToggleButton tglBtnMonthClosed;
    @FXML
    private GridPane rapportEditGrid;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TableView<TimeBooking> reportTable;
    @FXML
    private TableColumn<TimeBooking, String> projectNumberColumn;
    @FXML
    private TableColumn<TimeBooking, String> projectNameColumn;
    @FXML
    private TableColumn<TimeBooking, String> activityTypeColumn;
    @FXML
    private TableColumn<TimeBooking, String> roleColumn;
    @FXML
    private TableColumn<TimeBooking, String> descriptionColumn;
    @FXML
    private TableColumn<TimeBooking, Float> numberOfHoursColumn;

    private Date currendDate;
    private TimeBooking selectedTimeBooking;

    @FXML
    private ComboBox<Project> comboProject;
    @FXML
    private TextField txtDescription;
    @FXML
    private ComboBox<Role> comboRole;
    @FXML
    private ComboBox<ActivityType> comboActivityType;
    @FXML
    private TextField txtNumberOfHours;

    @FXML
    private Label totalHours;
    @FXML
    private Button btnAddReport;

    private ActivityTypeBll activityTypeBll = new ActivityTypeBll();
    private ProjectBll projectBll = new ProjectBll();
    private RoleBll roleBll = new RoleBll();
    private TimeBookingBll timeBookingBll = new TimeBookingBll();

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());

        currendDate = Calendar.getInstance().getTime();

        initCurrentDate();

        initComboCellRenderers();

        initComboProject();
        initComboRole();
        initComboActivityType();

        initTimeBookingsTableColumns();
        initTimeBookings();
        initTimeBookingsTableContextMenu();
        initTimeBookingsTableEvents();

        initDateChangeEvent();
    }

    private void initDateChangeEvent() {
        datePicker.setOnAction(event -> {
            if (datePicker.getValue() != null) {
                initTimeBookings();
            }
        });
    }

    private void initTimeBookingsTableEvents() {
        reportTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        selectedTimeBooking = newValue;
                        txtDescription.setText(newValue.getDescription());
                        txtNumberOfHours.setText(String.valueOf(newValue.getNumberOfHours()));

                        int index = 0;
                        for (Project project : comboProject.getItems()) {
                            if (project.getId() == newValue.getProject().getId()) {
                                comboProject.getSelectionModel().select(index);
                                break;
                            }
                            index++;
                        }

                        index = 0;
                        for (ActivityType activityType : comboActivityType.getItems()) {
                            if (activityType.getId() == newValue.getActivityType().getId()) {
                                comboActivityType.getSelectionModel().select(index);
                                break;
                            }
                            index++;
                        }

                        index = 0;
                        for (Role role : comboRole.getItems()) {
                            if (role.getId() == newValue.getRole().getId()) {
                                comboRole.getSelectionModel().select(index);
                                break;
                            }
                            index++;
                        }

                        btnAddReport.setText("ändern");
                    } else {
                        selectedTimeBooking = null;
                        resetInsertControlls();
                        btnAddReport.setText("hinzufügen");
                    }
                });

        reportTable.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ESCAPE)) {
                if (reportTable.getSelectionModel().getSelectedIndex() != -1) {
                    reportTable.getSelectionModel().clearSelection();
                }
            }
        });
    }

    private void initTimeBookingsTableContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Löschen");
        menu.getItems().add(removeMenuItem);
        reportTable.setContextMenu(menu);

        removeMenuItem.setOnAction((event) -> {
            TimeBooking selectedTimeBooking = reportTable.getSelectionModel().getSelectedItem();
            try {
                String msg = "Wirklich löschen?";
                Optional<ButtonType> result = AlertHelper.showConfirm("Bestätige", msg);
                if (result.get() == ButtonType.OK) {
                    timeBookingBll.deleteTimeBooking(selectedTimeBooking);
                    //initProjects(); // Todo: unschön, ev. refactoring der Architektur
                }
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
        });
    }

    private void initTimeBookingsTableColumns() {
        projectNumberColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().projectProperty().getValue() == null ?
                    null : cellData.getValue().projectProperty().getValue().projectNumberProperty();
        });
        projectNameColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().projectProperty().getValue() == null ?
                    null : cellData.getValue().projectProperty().getValue().nameProperty();
        });
        activityTypeColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().activityTypeProperty().getValue() == null ?
                    null : cellData.getValue().activityTypeProperty().getValue().nameProperty();
        });
        roleColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().roleProperty().getValue() == null
                    ? null : cellData.getValue().roleProperty().getValue().nameProperty();
        });
        descriptionColumn.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        numberOfHoursColumn.setCellValueFactory(cellData -> cellData.getValue().numberOfHoursProperty().asObject());
    }

    private void initTimeBookings() {
        reportTable.setItems(timeBookingBll.getTimeBookingsByDate(datePicker.getValue()));
        sumNumberOfHours();
    }

    private void initComboCellRenderers() {
        comboProject.setButtonCell(new ProjectListCellRenderer());
        comboProject.setCellFactory(new Callback<ListView<Project>, ListCell<Project>>() {
            @Override
            public ListCell<Project> call(ListView<Project> param) {
                return new ProjectListCellRenderer();
            }
        });

        comboActivityType.setButtonCell(new ActivityTypeListCellRenderer());
        comboActivityType.setCellFactory(new Callback<ListView<ActivityType>, ListCell<ActivityType>>() {
            @Override
            public ListCell<ActivityType> call(ListView<ActivityType> param) {
                return new ActivityTypeListCellRenderer();
            }
        });

        comboRole.setButtonCell(new RoleListCellRenderer());
        comboRole.setCellFactory(new Callback<ListView<Role>, ListCell<Role>>() {
            @Override
            public ListCell<Role> call(ListView<Role> param) {
                return new RoleListCellRenderer();
            }
        });

    }

    void initComboProject() {
        comboProject.setItems(projectBll.getAllProjects());
    }

    void initComboRole() {
        comboRole.setItems(roleBll.getAllRoles());
    }

    private void initComboActivityType() {
        comboActivityType.setItems(activityTypeBll.getAllActivityTypes());
    }

    private void initCurrentDate() {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(currendDate);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate = LocalDate.parse(date, formatter);
        datePicker.setValue(localDate);
    }

    @FXML
    private void btnCreateRapportClicked() {
        ReportGeneratorBll.writeReportAsPdf(datePicker.getValue());
        initTimeBookings();
    }

    @FXML
    private void toggleBtnCloseMonthClicked() {
        boolean toggleState = tglBtnMonthClosed.isSelected();
        rapportEditGrid.setDisable(toggleState);
        reportTable.setDisable(toggleState);

        if (toggleState) {
            tglBtnMonthClosed.setText("Monat entsperren");
        } else {
            tglBtnMonthClosed.setText("Monat abschliessen");
        }
    }

    @FXML
    private void btnAddNewReportEntryClicked() {
        TimeBooking timeBooking;
        if (selectedTimeBooking == null) {
            String description = txtDescription.getText();
            Float numberOfHours = 0f;
            try {
                numberOfHours = Float.parseFloat(txtNumberOfHours.getText());
            } catch (NumberFormatException e) {
                AlertHelper.showError("Error", "Fehlerhafte Eingabe");
                return;
            }
            LocalDate localDate = datePicker.getValue();

            Project project = comboProject.getValue();
            ActivityType activityType = comboActivityType.getValue();
            Role role = comboRole.getValue();

            timeBooking = new TimeBooking(description, localDate, numberOfHours, project, activityType, role);
        } else {
            timeBooking = selectedTimeBooking;
        }

        if (timeBookingBll.timeBookingIsValid(timeBooking)) {
            try {
                timeBookingBll.saveTimeBooking(timeBooking);
                sumNumberOfHours();
                resetInsertControlls();
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
        } else {
            AlertHelper.showError("Error", "Fehlerhafte Eingabe");
        }
    }

    private void sumNumberOfHours() {
        float total = 0f;
        for (TimeBooking item : reportTable.getItems()) {
            total += item.getNumberOfHours();
        }
        totalHours.setText(String.valueOf(total));
    }

    private void resetInsertControlls() {
        txtNumberOfHours.clear();
        txtDescription.clear();
        comboProject.getSelectionModel().clearSelection();
        comboActivityType.getSelectionModel().clearSelection();
        comboRole.getSelectionModel().clearSelection();
    }

}
