package app.controller;

import app.MainApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Antic Marjan on 09.03.2017.
 */
class MenuContentLoader {
    private static BorderPane navigationLayout = null;
    private static String currentTemplate = "";

    private static int counter = 0;

    MenuContentLoader() {
        super();
    }

    void initNavigationLayout() {
        FXMLLoader loader = TemplateData.viewLoader(TemplateData.appPath + "NavigationView.fxml");
        try {
            navigationLayout = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void showLoginView() {
        try {
            FXMLLoader loader = TemplateData.viewLoader(TemplateData.viewPath + "LoginView.fxml");
            AnchorPane rootLayout = loader.load();

            Scene scene = new Scene(rootLayout);
            TemplateData.primaryStage.setScene(scene);
            TemplateData.primaryStage.setResizable(false);
            TemplateData.primaryStage.show();
            //RootController controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void showAppView() {
        Scene scene = new Scene(navigationLayout);
        TemplateData.primaryStage.setScene(scene);
    }

    void setAppContent(String viewFile) {
        if (!currentTemplate.equals(viewFile)) {
            currentTemplate = viewFile;
            FXMLLoader loader = TemplateData.viewLoader(TemplateData.modulesPath + viewFile);
            try {
                AnchorPane content = loader.load();
                navigationLayout.setCenter(content);
                System.out.println(viewFile + " has been loaded!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void reset() {
        currentTemplate = "";
    }

}
