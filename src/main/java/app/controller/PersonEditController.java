package app.controller;

import app.bll.AccountTypeBll;
import app.bll.PersonBll;
import app.bll.RoleBll;
import app.bll.TeamBll;
import app.controller.cell_renderer.AccountTypeListCellRenderer;
import app.controller.cell_renderer.RoleListCellRenderer;
import app.controller.cell_renderer.TeamListCellRenderer;
import app.model.EAccountType;
import app.model.Person;
import app.model.Role;
import app.model.Team;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

/**
 * Created by Antic Marjan on 12.03.2017.
 */
public class PersonEditController {
    @FXML
    private TextField txtPersonnelNumber;
    @FXML
    private TextField txtFirstName;
    @FXML
    private TextField txtLastName;
    @FXML
    private TextField txtStreet;
    @FXML
    private TextField txtStreetNumber;
    @FXML
    private TextField txtPostalCode;
    @FXML
    private TextField txtCity;
    @FXML
    private ComboBox<Role> comboRole;
    @FXML
    private ComboBox<Team> comboTeam;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtPassword;
    @FXML
    private ComboBox<EAccountType> comboEAccountType;

    private PersonBll personBll = new PersonBll();
    private AccountTypeBll accountTypeBll = new AccountTypeBll();
    private RoleBll roleBll = new RoleBll();
    private TeamBll teamBll = new TeamBll();

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
        initComboCellRenderers();
        initComboAccountType();
        initComboRole();
        initComboTeam();
    }

    private void initComboCellRenderers() {
        comboRole.setButtonCell(new RoleListCellRenderer());
        comboRole.setCellFactory(new Callback<ListView<Role>, ListCell<Role>>() {
            @Override
            public ListCell<Role> call(ListView<Role> param) {
                return new RoleListCellRenderer();
            }
        });

        comboEAccountType.setButtonCell(new AccountTypeListCellRenderer());
        comboEAccountType.setCellFactory(new Callback<ListView<EAccountType>, ListCell<EAccountType>>() {
            @Override
            public ListCell<EAccountType> call(ListView<EAccountType> param) {
                return new AccountTypeListCellRenderer();
            }
        });

        comboTeam.setButtonCell(new TeamListCellRenderer());
        comboTeam.setCellFactory(new Callback<ListView<Team>, ListCell<Team>>() {
            @Override
            public ListCell<Team> call(ListView<Team> param) {
                return new TeamListCellRenderer();
            }
        });
    }

    private void initComboAccountType() {
        comboEAccountType.setItems(accountTypeBll.getAllAccountTypes());
    }

    private void initComboRole() {
        comboRole.setItems(roleBll.getAllRoles());
    }

    private void initComboTeam() {
        comboTeam.setItems(teamBll.getAllTeams());
    }

    @FXML
    private void btnSaveClicked() {
        String personnelNumber = txtPersonnelNumber.getText();
        String firstName = txtFirstName.getText();
        String lastName = txtLastName.getText();
        String street = txtStreet.getText();
        String streetNumber = txtStreetNumber.getText();
        String postalCode = txtPostalCode.getText();
        String city = txtCity.getText();

        Role role = comboRole.getValue();
        String email = txtEmail.getText();
        String password = txtPassword.getText();

        EAccountType accountType = comboEAccountType.getValue();
        Team team = comboTeam.getValue();
        Person person = new Person(personnelNumber, firstName, lastName, email, street, streetNumber, postalCode, city, password, accountType, team);

        person.setRole(role);

        if (personBll.personIsValid(person)) {
            try {
                personBll.savePerson(person);
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
            new ModalLoader().closeDialog();
        } else {
            AlertHelper.showError("Error", "Fehlerhafte Eingabe");
        }
    }
}
