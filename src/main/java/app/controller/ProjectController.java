package app.controller;

import app.bll.ProjectBll;
import app.bll.TimeAccountBll;
import app.model.Project;
import app.model.TimeAccount;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.Optional;

/**
 * Created by Antic Marjan on 09.03.2017.
 */
public class ProjectController {

    @FXML
    private TableView<Project> projectTable;
    @FXML
    private TableColumn<Project, String> projectNumberColumn;
    @FXML
    private TableColumn<Project, String> projectNameColumn;
    @FXML
    private TableColumn<Project, String> projectDescriptionColumn;
    @FXML
    private TableColumn<Project, String> parentProjectColumn;
    @FXML
    private TableColumn<Project, String> timeAccountColumn;

    @FXML
    private TableView<TimeAccount> timeAccountTable;
    @FXML
    private TableColumn<TimeAccount, String> timeAccountNameColumn;
    @FXML
    private TableColumn<TimeAccount, Float> maxNumberOfHoursColumn;

    private ProjectBll projectBll = new ProjectBll();
    private TimeAccountBll timeAccountBll = new TimeAccountBll();

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
        initProjectTableColumns();
        initProjectTableContextMenu();
        initProjects();
        initTimeAccountTableColumns();
        initTimeAccountTableContextMenu();
        initTimeAccounts();
    }

    private void initTimeAccountTableContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Löschen");
        MenuItem editMenuItem = new MenuItem("Bearbeiten");
        menu.getItems().add(removeMenuItem);
        menu.getItems().add(editMenuItem);
        timeAccountTable.setContextMenu(menu);

        removeMenuItem.setOnAction((event) -> {
            TimeAccount selectedTimeAccount = timeAccountTable.getSelectionModel().getSelectedItem();
            try {
                String msg = "Wirklich löschen? Sämtliche Projekt im Zeitkonto werden auch gelöscht.";
                Optional<ButtonType> result = AlertHelper.showConfirm("Bestätige", msg);
                if (result.get() == ButtonType.OK) {
                    timeAccountBll.deleteTimeAccount(selectedTimeAccount);
                    initProjects(); // Todo: unschön, ev. refactoring der Architektur (ev. Lösung: TimeAccount has Projects?)
                }
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
        });

        editMenuItem.setOnAction((event) -> {
            // Todo: Edit implementieren
            TimeAccount selectedTimeAccount = timeAccountTable.getSelectionModel().getSelectedItem();
            AlertHelper.showInfo("App-Info", "wurde nicht implementiert");
            // selectedProject muss an den Controller übermittelt werden
            // new ModalLoader().showModal("TimeAccountEditModal.fxml");
        });
    }

    private void initProjectTableContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Löschen");
        MenuItem editMenuItem = new MenuItem("Bearbeiten");
        menu.getItems().add(removeMenuItem);
        menu.getItems().add(editMenuItem);
        projectTable.setContextMenu(menu);

        removeMenuItem.setOnAction((event) -> {
            Project selectedProject = projectTable.getSelectionModel().getSelectedItem();
            try {
                String msg = "Wirklich löschen? Eventuell vorhandene Sub-Projekt werden auch mitgelöscht.";
                Optional<ButtonType> result = AlertHelper.showConfirm("Bestätige", msg);
                if (result.get() == ButtonType.OK) {
                    projectBll.deleteProject(selectedProject);
                    initProjects(); // Todo: unschön, ev. refactoring
                }
            } catch (Exception e) {
                AlertHelper.showError("Error", e.getMessage());
            }
        });

        editMenuItem.setOnAction((event) -> {
            // Todo: Edit implementieren
            Project selectedProject = projectTable.getSelectionModel().getSelectedItem();
            AlertHelper.showInfo("App-Info", "wurde nicht implementiert");
            // selectedProject muss an den Controller übermittelt werden
            // new ModalLoader().showModal("ProjectEditModal.fxml"); // bei edit saveProject in BLL aufrufen
        });
    }

    private void initProjectTableColumns() {
        projectNumberColumn.setCellValueFactory(cellData -> cellData.getValue().projectNumberProperty());
        projectNameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        projectDescriptionColumn.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        parentProjectColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().parentProperty().getValue() == null
                    ? null : cellData.getValue().parentProperty().getValue().projectNumberProperty();
        });
        timeAccountColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().timeAccountProperty().getValue() == null
                    ? null : cellData.getValue().timeAccountProperty().getValue().nameProperty();
        });
    }

    private void initProjects() {
        projectTable.setItems(projectBll.getAllProjects());
    }

    private void initTimeAccounts() {
        timeAccountTable.setItems(timeAccountBll.getAllTimeAccounts());
    }

    @FXML
    private void btnAddNewProjectClicked() {
        if (timeAccountBll.hasTimeAccounts()) {
            new ModalLoader().showModal("ProjectEditModal.fxml");
        } else {
            String msg = "Um ein Projekt anlegen zu können, muss ein Zeitkonto existieren, dass zugewiesen werden kann!";
            AlertHelper.showInfo("Information", msg);
        }
    }

    private void initTimeAccountTableColumns() {
        timeAccountNameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        maxNumberOfHoursColumn.setCellValueFactory(cellData -> cellData.getValue().maxNumberOfHoursProperty().asObject());
    }

    @FXML
    private void btnAddNewTimeAccountClicked() {
        new ModalLoader().showModal("TimeAccountEditModal.fxml");
    }
}
