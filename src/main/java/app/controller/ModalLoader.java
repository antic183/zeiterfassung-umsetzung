package app.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
class ModalLoader {
    private static Stage dialogStage;

    ModalLoader() {
        if (dialogStage == null) {
            dialogStage = new Stage();
            dialogStage.setResizable(false);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(TemplateData.primaryStage);
        }
    }

    void showModal(String template) {
        try {
            FXMLLoader loader = TemplateData.viewLoader(TemplateData.modalPath + template);
            AnchorPane layout = loader.load();
            Scene scene = new Scene(layout);

            dialogStage.setScene(scene);
            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void closeDialog() {
        dialogStage.close();
    }
}
