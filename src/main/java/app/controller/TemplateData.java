package app.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class TemplateData {
    static Stage primaryStage;
    static final String viewPath = "/view/";
    static final String appPath = viewPath + "app/";
    static final String modulesPath = appPath + "modules/";
    static final String modalPath = modulesPath + "modals/";

    public TemplateData(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void showLoginView() {
        MenuContentLoader menuContentLoader = new MenuContentLoader();
        menuContentLoader.showLoginView();
    }

    Stage getPrimaryStage() {
        return primaryStage;
    }

    static FXMLLoader viewLoader(String path) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(TemplateData.class.getResource(path));
        return loader;
    }

}
