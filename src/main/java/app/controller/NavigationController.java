package app.controller;

import app.model.CurrentUser;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class NavigationController {

    @FXML
    private Button btnModuleRapport;
    @FXML
    private Button btnModulePerson;
    @FXML
    private Button btnModuleRroject;

    MenuContentLoader menuContentLoeader;

    @FXML
    private void initialize() {
        System.out.println("---> init " + getClass().getSimpleName());
        menuContentLoeader = new MenuContentLoader();

        if (!CurrentUser.getInstance().getAccountType().name().equals("ADMIN")) {
            hideAdminModules();
        }
    }


    @FXML
    private void btnPersonsClicked() {
        menuContentLoeader.setAppContent("Persons.fxml");
    }

    @FXML
    private void btnProjectsClicked() {
        menuContentLoeader.setAppContent("Projects.fxml");
    }

    @FXML
    private void btnRapportClicked() {
        menuContentLoeader.setAppContent("Report.fxml");
    }

    @FXML
    private void btnLogoutClicked() {
        menuContentLoeader.reset();
        menuContentLoeader.showLoginView();
        CurrentUser.removeCurrentUser();
    }

    private void hideAdminModules() {
        btnModulePerson.setVisible(false);
        btnModuleRroject.setVisible(false);
    }
}
