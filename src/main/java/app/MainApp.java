package app;

import app.bll.ReportGeneratorBll;
import app.controller.TemplateData;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Personenverwaltung");

        ReportGeneratorBll.setPrimaryStage(primaryStage);

        TemplateData templateData = new TemplateData(primaryStage);
        templateData.showLoginView();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

