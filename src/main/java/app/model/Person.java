package app.model;


import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class Person {
    //<editor-fold desc="Properties">
    private int id;

    private StringProperty personnelNumber;
    private StringProperty firstName;
    private StringProperty lastName;
    private StringProperty email;

    private StringProperty street;
    private StringProperty streetNumber;
    private StringProperty fullStreet; // street & number

    private StringProperty postalCode;
    private StringProperty city;
    private StringProperty fullCity; // postale code & city

    private StringProperty password;

    private ObjectProperty<Role> defaultRole;
    private ObjectProperty<Team> team;
    private ObjectProperty<EAccountType> accountType;
    //</editor-fold>

    //<editor-fold desc="Constructors">
    public Person(int id, String personnelNumber, String firstName, String lastName, String email, String street, String streetNumber, String postalCode, String city, String password, EAccountType accountType, Team team) {
        this.id = id;
        this.personnelNumber = new SimpleStringProperty(personnelNumber);
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.email = new SimpleStringProperty(email);
        this.street = new SimpleStringProperty(street);
        this.streetNumber = new SimpleStringProperty(streetNumber);
        this.fullStreet = new SimpleStringProperty(street + " " + streetNumber);
        this.postalCode = new SimpleStringProperty(postalCode);
        this.city = new SimpleStringProperty(city);
        this.fullCity = new SimpleStringProperty(postalCode + " " + city);
        this.password = new SimpleStringProperty(password);

        this.defaultRole = new SimpleObjectProperty<>(null);

        try {
            this.accountType = new SimpleObjectProperty<>(accountType);
            this.team = new SimpleObjectProperty<>(team);
        } catch (IllegalArgumentException e) {
            this.accountType = new SimpleObjectProperty<>();
            this.team = new SimpleObjectProperty<>();
        }
    }

    public Person(String personnelNumber, String firstName, String lastName, String email, String street, String streetNumber, String postalCode, String city, String password, EAccountType accountType, Team team) {
        this(-1, personnelNumber, firstName, lastName, email, street, streetNumber, postalCode, city, password, accountType, team);
    }
    //</editor-fold>

    //<editor-fold desc="id Methods">
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    //</editor-fold>-m m

    //<editor-fold desc="personnelNumber Methods">
    public String getPersonnelNumber() {
        return personnelNumber.get();
    }

    public void setPersonnelNumber(String personnelNumber) {
        this.personnelNumber.set(personnelNumber);
    }

    public StringProperty personnelNumberProperty() {
        return personnelNumber;
    }
    //</editor-fold>

    //<editor-fold desc="firstName Methods">
    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }
    //</editor-fold>

    //<editor-fold desc="lastName Methods">
    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }
    //</editor-fold>

    //<editor-fold desc="email Methods">
    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public StringProperty emailProperty() {
        return email;
    }
    //</editor-fold>

    //<editor-fold desc="street Methods">
    public String getStreet() {
        return street.get();
    }

    public void setStreet(String street) {
        this.street.set(street);
    }

    public StringProperty streetProperty() {
        return street;
    }
    //</editor-fold>

    //<editor-fold desc="streetNumber Methods">
    public String getStreetNumber() {
        return streetNumber.get();
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber.set(streetNumber);
    }

    public StringProperty streetNumberProperty() {
        return streetNumber;
    }
    //</editor-fold>

    //<editor-fold desc="fullStreet Methods">
    public String getFullStreet() {
        return fullStreet.get();
    }

    public void setFullStreet(String fullStreet) {
        this.fullStreet.set(fullStreet);
    }

    public StringProperty fullStreetProperty() {
        return fullStreet;
    }
    //</editor-fold>

    //<editor-fold desc="postalCode Methods">
    public String getPostalCode() {
        return postalCode.get();
    }

    public void setPostalCode(String postalCode) {
        this.postalCode.set(postalCode);
    }

    public StringProperty postalCodeProperty() {
        return postalCode;
    }
    //</editor-fold>

    //<editor-fold desc="city Methods">
    public String getCity() {
        return city.get();
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public StringProperty cityProperty() {
        return city;
    }
    //</editor-fold>

    //<editor-fold desc="fullCity Methods">
    public String getFullCity() {
        return fullCity.get();
    }

    public void setFullCity(String fullCity) {
        this.fullCity.set(fullCity);
    }

    public StringProperty fullCityProperty() {
        return fullCity;
    }
    //</editor-fold>

    //<editor-fold desc="password Methods">
    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public StringProperty passwordProperty() {
        return password;
    }
    //</editor-fold>

    //<editor-fold desc="defaultRole Methods">
    public Role getRole() {
        return defaultRole.get();
    }

    public void setRole(Role defaultRole) {
        this.defaultRole.set(defaultRole);
    }

    public ObjectProperty<Role> defaultRoleProperty() {
        return defaultRole;
    }
    //</editor-fold>

    //<editor-fold desc="team Methods">
    public Team getTeam() {
        return team.get();
    }

    public void setTeam(Team team) {
        this.team.set(team);
    }

    public ObjectProperty<Team> teamProperty() {
        return team;
    }
    //</editor-fold>

    //<editor-fold desc="accountType Methods">
    public EAccountType getAccountType() {
        return accountType.get();
    }

    public void setAccountType(EAccountType accountType) {
        this.accountType.set(accountType);
    }

    public ObjectProperty<EAccountType> accountTypeProperty() {
        return accountType;
    }
    //</editor-fold>

    @Override
    public String toString() {
        // Todo: bad solution. make a cell renderer to show personList[personnelNr, personnelNr, personnelNr, ..] in team overview
        return personnelNumber.get();
    }
}
