package app.model;

import javafx.beans.property.*;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class Project {
    private int id;
    private StringProperty name;
    private StringProperty projectNumber;
    private StringProperty description;
    private ObjectProperty<Project> parent;
    private ObjectProperty<TimeAccount> timeAccount;

    public Project(int id, String name, String projectNumber, String description) {
        this.id = id;
        this.name = new SimpleStringProperty(name);
        this.projectNumber = new SimpleStringProperty(projectNumber);
        this.description = new SimpleStringProperty(description);

        this.parent = new SimpleObjectProperty<>(null);
        this.timeAccount = new SimpleObjectProperty<>(null);
    }

    public Project(String name, String projectNumber, String description) {
        this(-1, name, projectNumber, description);
    }

    // id
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    // name
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    // projectNumber
    public String getProjectNumber() {
        return projectNumber.get();
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber.set(projectNumber);
    }

    public StringProperty projectNumberProperty() {
        return projectNumber;
    }

    // description
    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    // parent
    public Project getParent() {
        return parent.get();
    }

    public void setParent(Project parent) {
        this.parent.set(parent);
    }

    public ObjectProperty<Project> parentProperty() {
        return parent;
    }

    // timeAccount
    public TimeAccount getTimeAccount() {
        return timeAccount.get();
    }

    public void setTimeAccount(TimeAccount timeAccount) {
        this.timeAccount.set(timeAccount);
    }

    public ObjectProperty<TimeAccount> timeAccountProperty() {
        return timeAccount;
    }
}
