package app.model;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class Team {
    private int id;
    private StringProperty name;
    private StringProperty description;
    private ListProperty<Person> personList;

    public Team(int id, String name, String description) {
        this.id = id;
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description);

        this.personList = new SimpleListProperty<>(FXCollections.observableArrayList());
    }

    public Team(String name, String description) {
        this(-1, name, description);
    }

    // id
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    // name
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    // description
    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    // personList
    public List<Person> getPersonList() {
        return personList.get();
    }

    public void setPersonList(List<Person> personList) {
        this.personList.addAll(personList);
    }

    public void addPerson(Person person) {
        this.personList.add(person);
    }

    public ListProperty<Person> personListProperty() {
        return personList;
    }

}
