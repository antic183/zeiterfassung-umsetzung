package app.model;

/**
 * Created by Antic Marjan on 11.03.2017.
 */
public enum EAccountType {
    USER, ADMIN
}
