package app.model;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class TimeAccount {
    private int id;
    private StringProperty name;
    private FloatProperty maxNumberOfHours;

    public TimeAccount(int id, String name, float maxNumberOfHours) {
        this.id = id;
        this.name = new SimpleStringProperty(name);
        this.maxNumberOfHours = new SimpleFloatProperty(maxNumberOfHours);
    }

    public TimeAccount(String name, float maxNumberOfHours) {
        this(-1, name, maxNumberOfHours);
    }

    // id
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    // name
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    // maxNumberOfHours
    public float getMaxNumberOfHours() {
        return maxNumberOfHours.get();
    }

    public void setMaxNumberOfHours(float numberOfHours) {
        this.maxNumberOfHours.set(numberOfHours);
    }

    public FloatProperty maxNumberOfHoursProperty() {
        return maxNumberOfHours;
    }
}
