package app.model;

import javafx.beans.property.*;

import java.time.LocalDate;

/**
 * Created by Antic Marjan on 18.03.2017.
 */
public class TimeBooking {
    private int id;
    private StringProperty description;
    private ObjectProperty<LocalDate> date;
    private FloatProperty numberOfHours;

    private ObjectProperty<Project> project;
    private ObjectProperty<ActivityType> activityType;
    private ObjectProperty<Role> role;


    public TimeBooking(int id, String description, LocalDate date, float numberOfHours, Project project, ActivityType activityType, Role role) {
        this.id = id;
        this.description = new SimpleStringProperty(description);
        this.date = new SimpleObjectProperty<>(date);
        this.numberOfHours = new SimpleFloatProperty(numberOfHours);

        this.project = new SimpleObjectProperty<>(project);
        this.activityType = new SimpleObjectProperty<>(activityType);
        this.role = new SimpleObjectProperty<>(role);

    }

    public TimeBooking(String description, LocalDate date, float numberOfHours, Project project, ActivityType activityType, Role role) {
        this(-1, description, date, numberOfHours, project, activityType, role);
    }

    // id
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    // description
    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    // date
    public LocalDate getDate() {
        return date.get();
    }

    public void setDate(LocalDate date) {
        this.date.set(date);
    }

    public ObjectProperty<LocalDate> dateProperty() {
        return date;
    }

    // numberOfHours
    public float getNumberOfHours() {
        return numberOfHours.get();
    }

    public void setNumberOfHours(float numberOfHours) {
        this.numberOfHours.set(numberOfHours);
    }

    public FloatProperty numberOfHoursProperty() {
        return numberOfHours;
    }

    // project
    public Project getProject() {
        return project.get();
    }

    public void setProject(Project project) {
        this.project.set(project);
    }

    public ObjectProperty<Project> projectProperty() {
        return project;
    }

    // activityType
    public ActivityType getActivityType() {
        return activityType.get();
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType.set(activityType);
    }

    public ObjectProperty<ActivityType> activityTypeProperty() {
        return activityType;
    }

    // role
    public Role getRole() {
        return role.get();
    }

    public void setRole(Role role) {
        this.role.set(role);
    }

    public ObjectProperty<Role> roleProperty() {
        return role;
    }
}
