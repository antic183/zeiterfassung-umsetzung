package app.model;

/**
 * Created by Antic Marjan on 13.03.2017.
 */
public class CurrentUser {
    private static CurrentUser instance;
    private EAccountType accountType;

    private int id;
    private String username;

    private CurrentUser(int id, String accountType, String username) {
        try {
            this.id = id;
            this.accountType = EAccountType.valueOf(accountType.toUpperCase());
            this.username = username;
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
    }

    public static final CurrentUser getInstance(int id, String accountType, String username) {
        if (instance == null) {
            instance = new CurrentUser(id, accountType, username);
        }

        return instance;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public static final CurrentUser getInstance() {
        return instance;
    }

    public EAccountType getAccountType() {
        return accountType;
    }

    public static void removeCurrentUser() {
        instance = null;
    }
}
