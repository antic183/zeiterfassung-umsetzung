package app.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class Role {
    private int id;
    private StringProperty name;

    public Role(int id, String name) {
        this.id = id;
        this.name = new SimpleStringProperty(name);
    }

    // id
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    // name
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }
}
