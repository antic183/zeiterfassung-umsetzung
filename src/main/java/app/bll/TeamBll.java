package app.bll;

import app.dal.TeamDb;
import app.model.Team;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static app.bll.ValidationHelper.stringIsNotEmptyOrNull;

/**
 * Created by Antic Marjan on 12.03.2017.
 */
public class TeamBll {

    protected static ObservableList<Team> teams;

    public TeamBll() {
        if (teams == null) {
            teams = FXCollections.observableArrayList();
        }
    }

    public ObservableList<Team> getAllTeams() {
        teams.clear();
        teams.addAll(TeamDb.getAllTeams());
        return teams;
    }

    public boolean hasTeams() {
        return teams.size() > 0;
    }

    public boolean teamIsValid(Team team) {
        return stringIsNotEmptyOrNull(team.getName()) && stringIsNotEmptyOrNull(team.getDescription());
    }

    public void saveTeam(Team team) throws Exception {
        if (team.getId() == -1) {
            // add
            TeamDb.addTeam(team);
            teams.add(team);
        } else {
            // update
            TeamDb.updateTeam(team);
            // Todo: update list persons
        }
    }

    public void deleteTeam(Team team) throws Exception {
        try {
            TeamDb.deleteTeam(team);
            teams.remove(team);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
