package app.bll;

import app.dal.TimeBookingDb;
import app.model.TimeBooking;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;

import static app.bll.ValidationHelper.stringIsNotEmptyOrNull;

/**
 * Created by Antic Marjan on 18.03.2017.
 */
public class TimeBookingBll {
    protected static ObservableList<TimeBooking> timeBookings;

    public TimeBookingBll() {
        if (timeBookings == null) {
            timeBookings = FXCollections.observableArrayList();
        }
    }

    public ObservableList<TimeBooking> getTimeBookingsByDate(LocalDate localDate) {
        timeBookings.clear();
        timeBookings.addAll(TimeBookingDb.getTimeBookingsByDate(localDate));
        return timeBookings;
    }

    public boolean timeBookingIsValid(TimeBooking timeBooking) {
        return stringIsNotEmptyOrNull(timeBooking.getDescription())
                && timeBooking.getDescription().trim().length() <= 250
                && timeBooking.getDate() != null
                && timeBooking.getNumberOfHours() != 0f
                && timeBooking.getNumberOfHours() % 0.5 == 0
                && timeBooking.getRole() != null
                && timeBooking.getActivityType() != null
                && timeBooking.getProject() != null;
    }

    public void saveTimeBooking(TimeBooking timeBooking) throws Exception {
        if (timeBooking.getId() == -1) {
            // add
            TimeBookingDb.addTimeBooking(timeBooking);
            timeBookings.add(timeBooking);
        } else {
            // update
            TimeBookingDb.updateTimeBooking(timeBooking);
            // Todo: update list activityTypes
        }
    }

    public void deleteTimeBooking(TimeBooking timeBooking) throws Exception {
        try {
            TimeBookingDb.deleteTimeBooking(timeBooking);
            timeBookings.remove(timeBooking);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
