package app.bll;

import app.dal.RoleDb;
import app.model.Role;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Antic Marjan on 12.03.2017.
 */
public class RoleBll {
    protected static ObservableList<Role> roles;

    public RoleBll() {
        if (roles == null) {
            roles = FXCollections.observableArrayList();
        }
    }

    public ObservableList<Role> getAllRoles() {
        roles.clear();
        roles.addAll(RoleDb.getAllRoles());
        return roles;
    }
}
