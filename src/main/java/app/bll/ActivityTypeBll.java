package app.bll;

import app.dal.ActivityTypeDb;
import app.model.ActivityType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Antic Marjan on 18.03.2017.
 * info: die Klasse ActivityType entspricht der Leistungsart
 */
public class ActivityTypeBll {

    protected static ObservableList<ActivityType> activityTypes;

    public ActivityTypeBll() {
        if (activityTypes == null) {
            activityTypes = FXCollections.observableArrayList();
        }
    }

    public ObservableList<ActivityType> getAllActivityTypes() {
        activityTypes.clear();
        activityTypes.addAll(ActivityTypeDb.getAllActivityTypes());
        return activityTypes;
    }
}
