package app.bll;

import app.dal.TimeBookingDb;
import app.model.CurrentUser;
import app.model.TimeBooking;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sun.util.resources.LocaleData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Antic Marjan on 19.03.2017.
 */
public class ReportGeneratorBll {
    private static Stage primaryStage;

    public static final void setPrimaryStage(Stage stage) {
        primaryStage = stage;
    }

    public static final void writeReportAsPdf(LocalDate date) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Report as PDF");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM-YYYY", Locale.getDefault());
        String title = date.format(formatter);

        fileChooser.setInitialFileName("Rapport-" + title + ".pdf");
        File file = fileChooser.showSaveDialog(primaryStage);

        if (file != null) {
            writeReport(file, title, date);
            System.out.println("gespeichert");
        } else {
            System.out.println("abgebrochen");
        }
    }

    private static void writeReport(File file, String title, LocalDate date) {
        TimeBookingBll timeBookingBll = new TimeBookingBll();
        List<List<String>> contentList = new ArrayList<>();

        float totalNumberOfHours = 0f;
        for (int i = 1; i <= date.lengthOfMonth(); i++) {
            LocalDate localDate = date.withDayOfMonth(i);
            List<TimeBooking> timeBookingsFromDate = timeBookingBll.getTimeBookingsByDate(localDate);
            List<String> tmp = new ArrayList<>();
            for (TimeBooking timeBooking : timeBookingsFromDate) {
                tmp.add(timeBooking.getDate().format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));
                tmp.add(timeBooking.getProject().getProjectNumber());
                tmp.add(timeBooking.getProject().getName());
                tmp.add(timeBooking.getProject().getDescription());
                tmp.add("" + timeBooking.getNumberOfHours());
                totalNumberOfHours += timeBooking.getNumberOfHours();
            }
            contentList.add(tmp);
        }

        String[] headers = new String[]{"Datum", "Projektnummer", "Projektname", "Beschreibung", "Anzahl Stunden"};
        Document document = new Document(PageSize.LETTER.rotate(), 20, 20, 20, 20);

        try {
            PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();

            Font fontHeader = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
            Font fontRow = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);


            PdfPTable table = new PdfPTable(headers.length);
            // first row
            PdfPCell cell = new PdfPCell(new Phrase("User: " + CurrentUser.getInstance().getUsername()));
            cell.setFixedHeight(20);
            cell.setColspan(5);
            table.addCell(cell);
            // second row
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM YYYY", Locale.getDefault());
            cell = new PdfPCell(new Phrase("Rapport vom: " + date.format(formatter)));
            cell.setFixedHeight(20);
            cell.setColspan(5);
            table.addCell(cell);

            for (String header : headers) {
                cell = new PdfPCell();
                cell.setGrayFill(0.9f);
                cell.setPhrase(new Phrase(header, fontHeader));
                table.addCell(cell);
            }
            table.completeRow();

            for (List<String> row : contentList) {
                for (String data : row) {
                    Phrase phrase = new Phrase(data, fontRow);
                    table.addCell(new PdfPCell(phrase));
                }
                table.completeRow();
            }

            // last row
            cell = new PdfPCell(new Phrase("Gesamttotal = " + totalNumberOfHours + " Stunden"));
            cell.setFixedHeight(20);
            cell.setColspan(5);
            table.addCell(cell);

            document.addTitle(title);
            document.add(table);
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }
}
