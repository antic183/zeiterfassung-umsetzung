package app.bll;

import app.dal.PersonDb;
import app.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static app.bll.ValidationHelper.stringIsNotEmptyOrNull;

/**
 * Created by Antic Marjan on 11.03.2017.
 */
public class PersonBll {

    protected static ObservableList<Person> persons;

    public PersonBll() {
        if (persons == null) {
            persons = FXCollections.observableArrayList();
        }
    }

    public ObservableList<Person> getAllPersons() {
        persons.clear();
        persons.addAll(PersonDb.getAllPersons());
        return persons;
    }

    public boolean personIsValid(Person person) {
        return stringIsNotEmptyOrNull(person.getPersonnelNumber())
                && stringIsNotEmptyOrNull(person.getFirstName())
                && stringIsNotEmptyOrNull(person.getLastName())
                && stringIsNotEmptyOrNull(person.getFullStreet())
                && stringIsNotEmptyOrNull(person.getFullCity())
                && stringIsNotEmptyOrNull(person.getEmail())
                && stringIsNotEmptyOrNull(person.getPassword())
                && person.getAccountType() != null;
    }

    public void savePerson(Person person) throws Exception {
        if (person.getId() == -1) {
            // add
            PersonDb.addPerson(person);
            persons.add(person);
        } else {
            // update
            PersonDb.updateProject(person);
            // Todo: update list activityTypes
        }
    }

    public void deleteProject(Person person) throws Exception {
        try {
            PersonDb.deletePerson(person);
            persons.remove(person);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
