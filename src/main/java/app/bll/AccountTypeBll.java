package app.bll;

import app.model.EAccountType;
import app.model.TimeAccount;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Antic Marjan on 12.03.2017.
 */
public class AccountTypeBll {
    private static ObservableList<EAccountType> accountTypes;

    public AccountTypeBll() {
        if (accountTypes == null) {
            accountTypes = FXCollections.observableArrayList(EAccountType.values());
        }
    }

    public ObservableList<EAccountType> getAllAccountTypes() {
        return accountTypes;
    }
}
