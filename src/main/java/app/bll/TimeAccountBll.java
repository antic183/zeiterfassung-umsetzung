package app.bll;

import app.dal.TimeAccountDb;
import app.model.TimeAccount;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static app.bll.ValidationHelper.stringIsNotEmptyOrNull;

/**
 * Created by Antic Marjan on 11.03.2017.
 */
public class TimeAccountBll {

    private static ObservableList<TimeAccount> timeAccounts;

    public TimeAccountBll() {
        if (timeAccounts == null) {
            timeAccounts = FXCollections.observableArrayList();
        }
    }

    public ObservableList<TimeAccount> getAllTimeAccounts() {
        timeAccounts.clear();
        timeAccounts.addAll(TimeAccountDb.getAllTimeAccounts());
        return timeAccounts;
    }

    public boolean hasTimeAccounts() {
        return timeAccounts.size() > 0;
    }

    public void saveTimeAccount(TimeAccount timeAccount) throws Exception {
        if (timeAccount.getId() == -1) {
            // add
            TimeAccountDb.addTimeAccount(timeAccount);
            timeAccounts.add(timeAccount);
        } else {
            // update
            TimeAccountDb.updateTimeAccount(timeAccount);
            // Todo: update list timeAccounts
        }
    }

    public boolean timeAccountIsValid(String name, String maxNumberOfHours) {
        try {
            Float floatNr = Float.parseFloat(maxNumberOfHours);
        } catch (NumberFormatException e) {
            return false;
        }

        return stringIsNotEmptyOrNull(name)
                && stringIsNotEmptyOrNull(maxNumberOfHours);
    }

    public void deleteTimeAccount(TimeAccount timeAccount) throws Exception {
        try {
            TimeAccountDb.deleteTimeAccount(timeAccount);
            timeAccounts.remove(timeAccount);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
