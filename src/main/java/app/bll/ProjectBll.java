package app.bll;

import app.dal.ProjectDb;
import app.model.Project;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static app.bll.ValidationHelper.stringIsNotEmptyOrNull;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class ProjectBll {

    protected static ObservableList<Project> projects;

    public ProjectBll() {
        if (projects == null) {
            projects = FXCollections.observableArrayList();
        }
    }

    public ObservableList<Project> getAllProjects() {
        projects.clear();
        projects.addAll(ProjectDb.getAllProjects());
        return projects;
    }

    public boolean hasProjects() {
        return projects.size() > 0;
    }

    public void saveProject(Project project) throws Exception {
        if (project.getId() == -1) {
            // add
            ProjectDb.addProject(project);
            projects.add(project);
        } else {
            // update
            ProjectDb.updateProject(project);
            // Todo: update list projects
        }
    }

    public boolean projectIsValid(Project project) {
        return stringIsNotEmptyOrNull(project.getName())
                && stringIsNotEmptyOrNull(project.getDescription())
                && stringIsNotEmptyOrNull(project.getProjectNumber())
                && project.getTimeAccount() != null;
    }

    public void deleteProject(Project project) throws Exception {
        try {
            ProjectDb.deleteProject(project);
            projects.remove(project);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
