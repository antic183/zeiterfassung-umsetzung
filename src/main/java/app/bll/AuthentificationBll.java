package app.bll;

import app.dal.AuthentificationDb;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

/**
 * Created by Antic Marjan on 09.03.2017.
 */
public class AuthentificationBll {
    public static boolean checkLogin(String username, String password) {
        boolean loggedIn = false;
        if (ValidationHelper.stringIsNotEmptyOrNull(username) && ValidationHelper.stringIsNotEmptyOrNull(password)) {
            String hashedPassword = Hashing.sha512().hashString(password, StandardCharsets.UTF_8).toString();
            loggedIn = AuthentificationDb.checkLogin(username, hashedPassword) ? true : false;
        }

        return loggedIn;
    }
}
