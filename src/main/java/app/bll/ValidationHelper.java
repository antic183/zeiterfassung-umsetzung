package app.bll;

/**
 * Created by Antic Marjan on 11.03.2017.
 */
class ValidationHelper {
    static boolean stringIsNotEmptyOrNull(String string) {
        return string != null && !string.trim().isEmpty();
    }
}
