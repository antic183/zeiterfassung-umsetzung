package app.dal;

import app.model.Project;
import app.model.TimeAccount;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class TimeAccountDb {

    private static final String TABLE_NAME = "zeitkonto";

    public static List<TimeAccount> getAllTimeAccounts() {
        List<TimeAccount> timeAccounts = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            String query = "SELECT id, Name, Stundenkontingent FROM " + TABLE_NAME;
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("Name");
                float maxNumberOfHours = rs.getFloat("Stundenkontingent");
                TimeAccount timeAccount = new TimeAccount(id, name, maxNumberOfHours);
                timeAccounts.add(timeAccount);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return timeAccounts;
        }
    }

    public static void addTimeAccount(TimeAccount timeAccount) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        String name = timeAccount.getName();
        Float maxNumberOfHours = timeAccount.getMaxNumberOfHours();

        try {
            String query = "INSERT INTO " + TABLE_NAME + "(Name, Stundenkontingent) values(?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, name);
            preparedStatement.setFloat(2, maxNumberOfHours);

            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int last_inserted_id = rs.getInt(1);
                timeAccount.setId(last_inserted_id);
            }
            rs.close();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void deleteTimeAccount(TimeAccount timeAccount) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        int id = timeAccount.getId();

        try {
            String query = "delete from " + TABLE_NAME + " where id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            int updatedRows = preparedStatement.executeUpdate();
            if (updatedRows == 0) {
                throw new Exception("delete query failed!");
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception("delete query failed!");
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void updateTimeAccount(TimeAccount timeAccount) {
        // Todo: implement db-update
        System.out.println("---UPDATE---");
    }
}
