package app.dal;

import app.model.Role;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic Marjan on 12.03.2017.
 */
public class RoleDb {
    private static final String TABLE_NAME = "rolle";

    public static List<Role> getAllRoles() {
        List<Role> roles = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();

        try {
            Statement stmt = connection.createStatement();
            String query = "select id, Name from " + TABLE_NAME;

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                // get team
                int id = rs.getInt("id");
                String name = rs.getString("Name");

                Role role = new Role(id, name);
                roles.add(role);
            }

            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return roles;
        }
    }
}
