package app.dal;

import app.bll.TeamBll;
import app.model.EAccountType;
import app.model.Person;
import app.model.Role;
import app.model.Team;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic Marjan on 11.03.2017.
 */
public class PersonDb {

    private static final String TABLE_NAME = "person";

    public static List<Person> getAllPersons() {
        List<Person> persons = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();

        try {
            Statement stmt = connection.createStatement();
            String query = "select p.id, p.Personalnummer, p.Name, p.Vorname, p.Email,  p.Strasse, p.Hausnummer"
                    + ", t.id, t.Name, t.Beschreibung"
                    + ", o.PLZ, o.Ort, g.Name, r.id, r.Name "
                    + "from " + TABLE_NAME + " p "
                    + "join Ort o "
                    + "on o.id=p.Ort_id "
                    + "join Gruppe g "
                    + "on g.id = p.Gruppe_id "
                    + "join Rolle_Person rp "
                    + "on rp.Person_id=p.id "
                    + "join Rolle r "
                    + "on r.id=rp.Rolle_id "
                    + "join Team t "
                    + "on t.id=p.team_id "
                    + "where rp.Default_Rolle=1";

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                // get project
                int id = rs.getInt("id");
                String personnelNumber = rs.getString("Personalnummer");
                String firstName = rs.getString("Name");
                String lastName = rs.getString("Vorname");
                String email = rs.getString("Email");
                String password = "****";
                String street = rs.getString("Strasse");
                String streetNumber = rs.getString("Hausnummer");
                String postalCode = rs.getString("PLZ");
                String city = rs.getString("Ort");
                EAccountType accountGroup = EAccountType.valueOf(rs.getString("g.Name").toUpperCase());

                int teamId = rs.getInt("t.id");
                String teamName = rs.getString("t.Name");
                String teamDescription = rs.getString("t.Beschreibung");
                Team team = new Team(teamId, teamName, teamDescription);

                Person person = new Person(id, personnelNumber, firstName, lastName, email, street, streetNumber, postalCode, city, password, accountGroup, team);

                int roleId = rs.getInt("r.id");
                String roleName = rs.getString("r.Name");
                Role role = new Role(roleId, roleName);
                person.setRole(role);

                persons.add(person);
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return persons;
        }
    }

    static List<Person> getAllPersonsByTeam(Team team) {
        List<Person> persons = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();

        try {
            String query = "select p.id, p.Personalnummer, p.Name, p.Vorname, p.Email,  p.Strasse, p.Hausnummer, p.team_id"
                    + ", o.PLZ, o.Ort, g.Name "
                    + "from " + TABLE_NAME + " p "
                    + "join Ort o "
                    + "on o.id=p.Ort_id "
                    + "join Gruppe g "
                    + "on g.id = p.Gruppe_id "
                    + "join team t "
                    + "on t.id=p.Team_id "
                    + "where t.id=(?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, team.getId());
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                // get activityTypes
                int id = rs.getInt("id");
                String personnelNumber = rs.getString("Personalnummer");
                String firstName = rs.getString("Name");
                String lastName = rs.getString("Vorname");
                String email = rs.getString("Email");
                String password = "****";
                String street = rs.getString("Strasse");
                String streetNumber = rs.getString("Hausnummer");
                String postalCode = rs.getString("PLZ");
                String city = rs.getString("Ort");
                EAccountType accountGroup = EAccountType.valueOf(rs.getString("g.Name").toUpperCase());

                Person person = new Person(id, personnelNumber, firstName, lastName, email, street, streetNumber, postalCode, city, password, accountGroup, team);

                persons.add(person);
            }
            preparedStatement.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return persons;
        }
    }

    public static void addPerson(Person person) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        connection.setAutoCommit(false); // transaction

        int existingCityId = -1;
        try {
            String query = "select id from Ort where PLZ=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, person.getPostalCode());

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                existingCityId = rs.getInt("id");
            }
            rs.close();
            preparedStatement.close();

            if (existingCityId == -1) {
                // insert new City, when city doesn't exists:
                String query2 = "insert into Ort(PLZ, Ort) values(?, ?)";
                PreparedStatement preparedStatement2 = connection.prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
                preparedStatement2.setString(1, person.getPostalCode());
                preparedStatement2.setString(2, person.getCity());

                preparedStatement2.execute();
                ResultSet rs2 = preparedStatement2.getGeneratedKeys();
                if (rs2.next()) {
                    existingCityId = rs2.getInt(1);
                } else {
                    throw new Exception("neuer Ort kann nicht hinzugefügt werden!");
                }
                preparedStatement2.close();
            }

            // insert new Person
            String query3 = "insert into " + TABLE_NAME
                    + "(Personalnummer, Name, Vorname, Email, Password, Strasse, Hausnummer, Team_id, Ort_id, Gruppe_id) "
                    + "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement preparedStatement3 = connection.prepareStatement(query3, Statement.RETURN_GENERATED_KEYS);
            preparedStatement3.setString(1, person.getPersonnelNumber());
            preparedStatement3.setString(2, person.getFirstName());
            preparedStatement3.setString(3, person.getLastName());
            preparedStatement3.setString(4, person.getEmail());
            preparedStatement3.setString(5, Hashing.sha512().hashString(person.getPassword(), StandardCharsets.UTF_8).toString());
            preparedStatement3.setString(6, person.getStreet());
            preparedStatement3.setString(7, person.getStreetNumber());
            preparedStatement3.setInt(8, person.teamProperty().get().getId());
            preparedStatement3.setInt(9, existingCityId);

            int accountId = person.getAccountType().equals(EAccountType.ADMIN) ? 1 : 2; // fake: 1 = Admin, 2 = User
            preparedStatement3.setInt(10, accountId);

            preparedStatement3.execute();
            ResultSet rs3 = preparedStatement3.getGeneratedKeys();
            if (rs3.next()) {
                int last_inserted_id = rs3.getInt(1);
                person.setId(last_inserted_id);
            }
            rs3.close();
            preparedStatement3.close();

            // insert new Role_Person
            String query4 = "insert into Rolle_Person(Default_Rolle, Rolle_id, Person_id) values(?, ?, ?)";

            PreparedStatement preparedStatement4 = connection.prepareStatement(query4);
            preparedStatement4.setInt(1, 1);
            preparedStatement4.setInt(2, person.getRole().getId());
            preparedStatement4.setInt(3, person.getId());

            int count = preparedStatement4.executeUpdate();
            if (count == 0) {
                System.err.println();
                throw new Exception("kein insert in Tabelle Rolle_Person!");
            }
            preparedStatement4.close();

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void deletePerson(Person person) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        try {
            String query = "delete from " + TABLE_NAME + " where id=(?)";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, person.getId());

            int count = preparedStatement.executeUpdate();
            if (count == 0) {
                System.err.println();
                throw new Exception("kein delete in Tabelle " + TABLE_NAME + "!");
            }
            preparedStatement.close();
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void updateProject(Person person) {
        // Todo: implement it!
    }
}
