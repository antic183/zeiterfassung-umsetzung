package app.dal;

import app.model.Team;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic Marjan on 12.03.2017.
 */
public class TeamDb {
    private static final String TABLE_NAME = "team";

    public static List<Team> getAllTeams() {
        List<Team> teams = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();

        try {
            Statement stmt = connection.createStatement();
            String query = "select id, Name, Beschreibung from " + TABLE_NAME;

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                // get team
                int id = rs.getInt("id");
                String name = rs.getString("Name");
                String description = rs.getString("Beschreibung");

                Team team = new Team(id, name, description);
                team.setPersonList(PersonDb.getAllPersonsByTeam(team));
                teams.add(team);
            }

            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return teams;
        }
    }

    public static void addTeam(Team team) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        String name = team.getName();
        String description = team.getDescription();

        try {
            String query = "INSERT INTO " + TABLE_NAME + "(Name, Beschreibung) values(?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, description);

            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int last_inserted_id = rs.getInt(1);
                team.setId(last_inserted_id);
            }
            rs.close();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void updateTeam(Team team) {
        // Todo: implement db-update
        System.out.println("---UPDATE---");
    }

    public static void deleteTeam(Team team) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        try {
            String query = "delete from " + TABLE_NAME + " where id=(?)";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, team.getId());

            int count = preparedStatement.executeUpdate();
            if (count == 0) {
                throw new Exception("kein delete in Tabelle " + TABLE_NAME + "!");
            }
            preparedStatement.close();
        } catch (SQLException e) {
            throw new Exception("Zuerst bitte alle Personen aus dem Team entfernen!\n\n\n" + e.getMessage());
        } finally {
            MySqlConnector.closeConnection();
        }
    }
}
