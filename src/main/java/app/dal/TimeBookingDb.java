package app.dal;

import app.model.*;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic Marjan on 18.03.2017.
 */
public class TimeBookingDb {

    private static final String TABLE_NAME = "zeitbuchung";

    public static List<TimeBooking> getTimeBookingsByDate(LocalDate localDate) {
        List<TimeBooking> timeBookings = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();

        try {
            String query = "select z.id, z.beschreibung, z.anzahl_stunden"
                    + ", l.id, l.Name, r.id, r.name"
                    + ", p.id, p.projektnummer, p.name, p.beschreibung"
                    + " from " + TABLE_NAME + " z"
                    + " join leistungsart l"
                    + " on l.id=z.leistungsart_id"
                    + " join rolle_person_projekt rpr"
                    + " on z.rolle_person_projekt_id=rpr.id"
                    + " join projekt p"
                    + " on rpr.projekt_id=p.id"
                    + " join rolle_person rp"
                    + " on rpr.rolle_person_id=rp.id"
                    + " join rolle r"
                    + " on rp.rolle_id=r.id"
                    + " where rp.person_id=? and z.datum=?"
                    + " order by z.datum";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, CurrentUser.getInstance().getId());
            preparedStatement.setDate(2, Date.valueOf(localDate));
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int timeBookingId = rs.getInt("z.id");
                String timeBookingDescription = rs.getString("z.beschreibung");
                float timeBookingNumberOfHours = rs.getFloat("z.anzahl_stunden");
                int activityTypeId = rs.getInt("l.id");
                String activityTypeName = rs.getString("l.name");
                int roleId = rs.getInt("r.id");
                String roleName = rs.getString("r.name");
                int projectId = rs.getInt("p.id");
                String projectNumber = rs.getString("p.projektnummer");
                String projectName = rs.getString("p.name");
                String projectDescription = rs.getString("p.beschreibung");

                Project project = new Project(projectId, projectName, projectNumber, projectDescription);
                ActivityType activityType = new ActivityType(activityTypeId, activityTypeName);
                Role role = new Role(roleId, roleName);

                TimeBooking timeBooking = new TimeBooking(timeBookingId, timeBookingDescription, localDate, timeBookingNumberOfHours, project, activityType, role);
                timeBookings.add(timeBooking);
            }
            rs.close();
            preparedStatement.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return timeBookings;
        }
    }

    public static void addTimeBooking(TimeBooking timeBooking) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        connection.setAutoCommit(false); // transaction
        Project project = timeBooking.getProject();
        Role role = timeBooking.getRole();
        ActivityType activityType = timeBooking.getActivityType();

        try {
            // ___ insert or get(when exists) rolle_person ___
            String query1 = "insert ignore into rolle_person(rolle_id, person_id) values(?, ?)";
            PreparedStatement preparedStatement1 = connection.prepareStatement(query1, Statement.RETURN_GENERATED_KEYS);
            preparedStatement1.setInt(1, role.getId());
            preparedStatement1.setInt(2, CurrentUser.getInstance().getId());
            preparedStatement1.execute();
            ResultSet rs1 = preparedStatement1.getGeneratedKeys();
            int rolePersonId = 0;
            if (rs1.next()) {
                // get new rolePersonId
                rolePersonId = rs1.getInt(1);
            } else {
                // get existing rolePersonId
                String queryGetRolePersonId = "select id from rolle_person where rolle_id=? and person_id=?";
                PreparedStatement preparedStatementGetRolePersonId = connection.prepareStatement(queryGetRolePersonId);
                preparedStatementGetRolePersonId.setInt(1, role.getId());
                preparedStatementGetRolePersonId.setInt(2, CurrentUser.getInstance().getId());
                ResultSet rsGetRolePersonId = preparedStatementGetRolePersonId.executeQuery();
                while (rsGetRolePersonId.next()) {
                    rolePersonId = rsGetRolePersonId.getInt("id");
                    break;
                }
                rsGetRolePersonId.close();
                preparedStatementGetRolePersonId.close();
            }
            rs1.close();
            preparedStatement1.close();

            // ___ insert or get(when exists) rolle_person_projekt ___
            String query2 = "insert ignore into rolle_person_projekt(rolle_person_id, projekt_id) values(?, ?)";
            PreparedStatement preparedStatement2 = connection.prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.setInt(1, rolePersonId);
            preparedStatement2.setInt(2, project.getId());
            preparedStatement2.execute();

            ResultSet rs2 = preparedStatement2.getGeneratedKeys();
            int rolePersonProjectId = 0;
            if (rs2.next()) {
                // get new rolePersonProjectId
                rolePersonProjectId = rs2.getInt(1);
            } else {
                // get existing rolePersonProjectId
                String queryGetRolePersonProjectId = "select id from rolle_person_projekt where rolle_person_id=? and projekt_id=?";
                PreparedStatement preparedStatementGetRolePersonProjectId = connection.prepareStatement(queryGetRolePersonProjectId);
                preparedStatementGetRolePersonProjectId.setInt(1, rolePersonId);
                preparedStatementGetRolePersonProjectId.setInt(2, project.getId());
                ResultSet rsGetRolePersonProjectId = preparedStatementGetRolePersonProjectId.executeQuery();
                while (rsGetRolePersonProjectId.next()) {
                    rolePersonProjectId = rsGetRolePersonProjectId.getInt("id");
                    break;
                }
                rsGetRolePersonProjectId.close();
                preparedStatementGetRolePersonProjectId.close();
            }
            rs2.close();
            preparedStatement2.close();

            // ___ insert into zeitbuchung ___
            String query3 = "insert into " + TABLE_NAME + "(datum, beschreibung, anzahl_stunden, rolle_person_projekt_id, leistungsart_id) values(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement3 = connection.prepareStatement(query3, Statement.RETURN_GENERATED_KEYS);

            preparedStatement3.setDate(1, Date.valueOf(timeBooking.getDate()));
            preparedStatement3.setString(2, timeBooking.getDescription());
            preparedStatement3.setFloat(3, timeBooking.getNumberOfHours());
            preparedStatement3.setInt(4, rolePersonProjectId);
            preparedStatement3.setInt(5, activityType.getId());
            preparedStatement3.execute();

            ResultSet rs3 = preparedStatement3.getGeneratedKeys();
            if (rs3.next()) {
                timeBooking.setId(rs3.getInt(1));
            } else {
                throw new Exception("SQL exception: insert into " + TABLE_NAME + ".");
            }

            rs3.close();
            preparedStatement3.close();

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void updateTimeBooking(TimeBooking timeBooking) throws Exception {
        // Todo: implement it!
        throw new Exception("noch nicht implementiert!");
    }

    public static void deleteTimeBooking(TimeBooking timeBooking) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        int id = timeBooking.getId();

        try {
            String query = "delete from " + TABLE_NAME + " where id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            int updatedRows = preparedStatement.executeUpdate();
            if (updatedRows == 0) {
                throw new Exception("delete query failed!");
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception("delete query failed!");
        } finally {
            MySqlConnector.closeConnection();
        }
    }
}
