package app.dal;

import app.model.ActivityType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic Marjan on 18.03.2017.
 */
public class ActivityTypeDb {

    private static final String TABLE_NAME = "leistungsart";

    public static List<ActivityType> getAllActivityTypes() {
        List<ActivityType> activityTypeList = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();

        try {
            Statement stmt = connection.createStatement();
            String query = "select id, name from " + TABLE_NAME;
            System.out.println(query);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                // get project
                int id = rs.getInt("id");
                String name = rs.getString("name");
                ActivityType project = new ActivityType(id, name);
                activityTypeList.add(project);
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return activityTypeList;
        }
    }
}
