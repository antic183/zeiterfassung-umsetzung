package app.dal;

import app.model.CurrentUser;

import java.sql.*;

/**
 * Created by Antic Marjan on 09.03.2017.
 */
public class AuthentificationDb {
    private static final String TABLE_NAME = "Person";

    public static boolean checkLogin(String username, String password) {
        boolean isLoggedIn = false;
        Connection connection = MySqlConnector.getConnection();

        try {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " p join Gruppe g on g.id=p.Gruppe_id WHERE Email=? AND Password=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                isLoggedIn = true;
                CurrentUser.getInstance(rs.getInt("id"), rs.getString("g.Name"), username);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return isLoggedIn;
        }
    }

//    public static void main(String[] args) throws SQLException {
//        boolean isLoggedIn = checkLogin("admin@example.com", "1234");
//        System.out.println(isLoggedIn);
//    }
}
