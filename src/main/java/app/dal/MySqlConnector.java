package app.dal;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Antic Marjan on 09.03.2017.
 */
class MySqlConnector {
    private static String URL;
    private static String USER_NAME;
    private static String PASSWORD;
    private static Connection connection;

    private static final void setConnection() {
        try {
            connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static final void loadDbConfig() {
        try (InputStream fis = MySqlConnector.class.getResourceAsStream("/config/DB_Config.xml")) {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document configDocument = documentBuilder.parse(fis);

            URL = configDocument.getElementsByTagName("url").item(0).getTextContent();
            USER_NAME = configDocument.getElementsByTagName("username").item(0).getTextContent();
            PASSWORD = configDocument.getElementsByTagName("password").item(0).getTextContent();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    static final Connection getConnection() {
        if (connection == null) {
            loadDbConfig();
            setConnection();
        }

        return connection;
    }

    static final void closeConnection() {
        try {
            connection.close();
            connection = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
