package app.dal;

import app.model.Project;
import app.model.TimeAccount;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic Marjan on 10.03.2017.
 */
public class ProjectDb {

    private static final String TABLE_NAME = "projekt";

    public static List<Project> getAllProjects() {
        List<Project> projects = new ArrayList<>();
        Connection connection = MySqlConnector.getConnection();

        try {
            Statement stmt = connection.createStatement();
            String query = "select p.id, p.projektnummer, p.name, p.beschreibung, p.parent_id, z.id, z.name, z.stundenkontingent from "
                    + TABLE_NAME + " p"
                    + " join zeitkonto z"
                    + " on z.id = p.zeitkonto_id";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                // get project
                int id = rs.getInt("p.id");
                String name = rs.getString("p.name");
                String projectNumber = rs.getString("p.projektnummer");
                String description = rs.getString("p.beschreibung");
                int timeAccountId = rs.getInt("z.id");
                String timeAccountName = rs.getString("z.name");
                float maxNumberOfHours = rs.getFloat("z.stundenkontingent");
                Project project = new Project(id, name, projectNumber, description);
                project.setTimeAccount(new TimeAccount(timeAccountId, timeAccountName, maxNumberOfHours));

                int parent_id = rs.getInt("parent_id");
                // get parent project
                if (parent_id != 0) {
                    PreparedStatement preparedStatement = connection.prepareStatement("select id, projektnummer, name, beschreibung from " + TABLE_NAME + " where id=?");
                    preparedStatement.setInt(1, parent_id);
                    ResultSet rs2 = preparedStatement.executeQuery();
                    while (rs2.next()) {
                        int ParentId = rs2.getInt("id");
                        String ParentName = rs2.getString("name");
                        String ParentProjectNumber = rs2.getString("projektnummer");
                        String ParentDescription = rs2.getString("beschreibung");
                        project.setParent(new Project(ParentId, ParentName, ParentProjectNumber, ParentDescription));
                    }
                    rs2.close();
                    preparedStatement.close();
                }


                projects.add(project);
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySqlConnector.closeConnection();
            return projects;
        }
    }

    public static void addProject(Project project) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        String projectNumber = project.getProjectNumber();
        String name = project.getName();
        String description = project.getDescription();
        int parentId = project.getParent() != null ? project.getParent().getId() : 0;
        int timeAccountId = project.getTimeAccount() != null ? project.getTimeAccount().getId() : 0;

        try {
            String query = "insert into " + TABLE_NAME + "(Projektnummer, Name, Beschreibung, Parent_id, Zeitkonto_id) values(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, projectNumber);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, description);
            if (parentId != 0) {
                preparedStatement.setInt(4, parentId);
            } else {
                preparedStatement.setNull(4, Types.INTEGER);
            }
            if (timeAccountId != 0) {
                preparedStatement.setInt(5, timeAccountId);
            } else {
                preparedStatement.setNull(5, Types.INTEGER);
            }
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int last_inserted_id = rs.getInt(1);
                project.setId(last_inserted_id);
            }
            rs.close();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void deleteProject(Project project) throws Exception {
        Connection connection = MySqlConnector.getConnection();
        int id = project.getId();

        try {
            String query = "delete from " + TABLE_NAME + " where id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            int updatedRows = preparedStatement.executeUpdate();
            if (updatedRows == 0) {
                throw new Exception("delete query failed!");
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception("delete query failed!");
        } finally {
            MySqlConnector.closeConnection();
        }
    }

    public static void updateProject(Project project) {
        // Todo: implement db-update
        System.out.println("---UPDATE---");
    }

    // update
    // delete
}
